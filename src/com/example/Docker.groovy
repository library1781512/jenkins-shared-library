#!/usr/bin/env groovy
package com.example
class Docker implements Serializable {
    def script
    Docker(script) {
        this.script =script
    }
    def buildJar() {
        script.echo "testing"
        script.sh "mvn clean package"
    }
    def buildImage(String image) {
        script.sh "docker build -t $image ."
    }
    def buildLogin() {
        script.withCredentials([script.usernamePassword(credentialsId: 'docker', usernameVariable: 'user', passwordVariable: 'pass')]) {
            script.sh "echo $script.pass | docker login -u $script.user --password-stdin"
        }
    }
    def buildPush(String image) {
        script.sh "docker push $image"
    }
    def buildPost(){
        script.withCredentials([script.usernamePassword(credentialsId:'Git',usernameVariable:'user',passwordVariable:'pass')]){
            script.sh "git remote set-url origin https://$script.user:$script.pass@gitlab.com/tayyabnisarkz/java-maven-app-version.git"
            script.sh'git add .'
            script.sh'git commit -m "new build true"'
            script.sh 'git status'
            script.sh'git push origin HEAD:jenkins-shared-lib'
        }
    }
}