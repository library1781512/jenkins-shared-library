#!/usr/bin/env groovy

import com.example.Docker

def call(String image) {
    return new Docker(this).buildPush(image)
}
